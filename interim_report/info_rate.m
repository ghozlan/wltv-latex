function R_vec = info_rate( H_TX, H_CH, H_RX, P_vec, SCHEME, SIM )

N0 = SIM.N0;
dt = SIM.dt;
REAL_DIM_PER_SYM = SIM.REAL_DIM_PER_SYM;
T_TRANSMISSION = SIM.T_TRANSMISSION;

%%

Sigma_X_NORMALIZED = power_alloc(H_TX, SCHEME, SIM);


%%
H_RXCHTX = H_RX' * H_CH * H_TX;

Sigma_N = N0/2 * REAL_DIM_PER_SYM * (1/dt);  % DEPENDS ON WHETHER NOISE IS REAL (PASSBAND) OR COMPLEX (BASEBAND)
trace(Sigma_N)*dt

Sigma_Z = H_RX' * Sigma_N * H_RX;
Sigma_Z = real(Sigma_Z); % real(...) to throw away the small imaginary numbers due to numerical errors
trace(Sigma_Z)*dt

I_vec = zeros(1,length(P_vec));
loop_index = 1;
for P = P_vec
% %Sigma_X = P*T_sym * eye(N_sym); %XXXXXXXXXXXXXXXXXXXXXXXXXXXX
% if(PASSBAND)
%     Sigma_X = P/(2*W) * eye(2*N_sym); %XXXXXXXXXXXXXXXXXXXXXXXXXXXX
% else
%     Sigma_X = P/W * eye(N_sym); %XXXXXXXXXXXXXXXXXXXXXXXXXXXX
% end
% Sigma_X = P/W * eye(N_sym_total); %XXXXXXXXXXXXXXXXXXXXXXXXXXXX
Sigma_X = P * Sigma_X_NORMALIZED; %XXXXXXXXXXXXXXXXXXXXXXXXXXXX
[trace((H_TX' * H_TX) * Sigma_X)*dt/T_TRANSMISSION P]   % CHECK POWER CONSTRAINT
Sigma_S = H_RXCHTX * Sigma_X * H_RXCHTX'; 
Sigma_S = real( Sigma_S );

I = ( sum(log(eig(Sigma_S + Sigma_Z))) - sum(log(eig(Sigma_Z))) );
%I_vec = [I_vec I];
I_vec(loop_index) = I;
loop_index = loop_index + 1;
end

R_vec = I_vec / 2 * REAL_DIM_PER_SYM / T_TRANSMISSION;

end

