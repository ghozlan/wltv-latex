\documentclass[journal]{IEEEtran}
%\onecolumn
\twocolumn
\input{header}

\begin{document}

\title{EE599 Underwater Acoustic Communication:\\Project Proposal}
\maketitle
%\input{some_file}

\begin{abstract}
A multi-scale multi-lag channel model for underwater acoustic communication is considered. 
This project follows up on the multi-layer scheme proposed by Xu \textit{et al.}
where the data symbols of different layers are modulated using different pulses at different rates and transmitted at different carriers.
The proposed scheme can ensure that there is no crosstalk between different layers at the the receiver if certain conditions are satisfied.
In this project, I will study the multi-layer scheme with some crosstalk permitted between layers 
which would allow using more layers compared to the no-crosstalk case. 
To evaluate the trade-off between quality degradation due to crosstalk between layers and the gain in data rate due to transmission of more layers,
information rates will be computed numerically.
\end{abstract}

\section{Introduction}
\label{sec:intro}
Relative motion between objects (transmitter, receiver and scatterers) in a medium 
causes the propagating signals to experience the so-called Doppler effect.
Given the velocity of motion $v$ and the propagation speed $c$ of the medium, 
the Doppler effect is a scaling (compression or dilation) to the signal with the value $a = \frac{c+v}{c-v}$.
For a complex exponential signal with a frequency $f_0$, the Doppler scaling $a$ is \emph{equivalent} to a frequency shift by $(a-1) f_0$.
For more general signals, the Doppler scaling $a$ can be \emph{approximated} by a fixed frequency shift $f_d = (a-1) f_c$, 
where $f_c$ is the center frequency of the signal, under certain conditions.
The conditions are
1) the motion is slow during the signal duration ( $(a-1) W T \ll 1$ where $W T$ is time-bandwidth product) and
2) the fractional bandwidth, i.e. the ratio of the bandwidth $W$ to the center frequency $f_c$, is small ($W/f_c < 0.1$).
%Under such conditions, the communication channel can be modeled as a narrowband linear time-varying (LTV) channel.

These conditions are not satisfied in underwater acoustic communication 
because the relatively low speed of sound ($v/c$ ranges from $10^{-3}$ to $10^{-1}$) 
and because the center frequency is of the same order of the signal bandwidth.
Therefore, a Doppler scaling is not well approximated by a fixed frequency shift.
Because of the multipath nature of wireless communication, 
the propagating signal may experience different Doppler scalings across different paths 
giving rise to a \emph{multi-scale} channel.
If the channel is frequency-selective in addition to being time-varying (such a channel is said to be doubly-selective),
then the channel can be modeled by the so-called multi-scale multi-lag (MSML) model \cite{MSML-TSP2006}, \cite{MSML-TSP2013}.
%The communication channel, in this case, is better modeled as a wideband linear time-varying (LTV) channel.

Xu \textit{et al.} studied in \cite{MSML-TSP2013} a multi-layer transmission scheme, where in each layer, 
the transmit data symbols are modulated by a different pulse at a different rate, and
up-converted to a different carrier frequency. 
At the receiver, the received signal is applied to a bank of filters (after down-conversion using different carriers), 
then sampled at the appropriate rate at each filter output.
It was shown in \cite{MSML-TSP2013} that it is possible to eliminate the crosstalk between different layers (different scales) under certain conditions.
The goal in this project is to investigate the multi-layer transmission scheme where crosstalk is permitted between layers.
The metric used to compare the crosstalk-free scheme to the scheme with crosstalk is the achievable information rate.

The MSML model and the multi-layer scheme are briefly described in Sec. \ref{sec:ch-model} and  Sec. \ref{sec:multi-layer}, respectively.
Sec. \ref{sec:info_rates} outlines the computation of information rates.

\section{Channel Model}
\label{sec:ch-model}
Consider the wideband linear time-varying (LTV) channel in which the received signal $r(t)$ is related to the transmitted signal $x(t)$ through
\begin{align}
r(t) = \iint \chi(\alpha,\tau) \sqrt{\alpha} x(\alpha(t-\tau)) d\alpha d\tau + w(t)
\label{eq:rt_wltv}
\end{align}
where $\chi(\alpha,\tau)$ is the wideband spreading function and 
$w(t)$ is a realization of a white Gaussian process with single-sided power spectral density $N_0$.
Equation (\ref{eq:rt_wltv}) is a multi-scale multi-lag model, in general.

Suppose the transmitted signal $x(t)$ is
\begin{align}
x(t) = \Re\left\{ x_b(t) e^{j 2\pi f_c t} \right\}
\end{align}
where 
$x_b(t)$ is a baseband signal with frequency support $[-\frac{W_\star}{2},\frac{W_\star}{2}]$ and 
$f_c$ is the carrier frequency.
In \cite{MSML-TSP2013}, the authors derived a time-scale decomposition of (\ref{eq:rt_wltv})
\begin{align}
r(t) \approx \Re\left\{
\sum_r e^{j 2\pi f_c a_r t} 
\sum_\ell \hat{\chi}_b(r,\ell) ~\sqrt{a_r} x_b(a_r t-T_\ell)
\right\}
+ w(t)
\label{eq:rt_approx}
\end{align}
where $a_r = a_\star^r$, $T_\ell = \ell T_\star$ and
%Define $x_{r,\ell}(t) = \sqrt{a_r} ~ x(a_r(t-T_\ell))$.
\begin{align}
\hat{\chi}_b(r,\ell) 
&= 
\iint \chi(\alpha^\prime,\tau^\prime) e^{-j 2\pi f_c a_r \tau^\prime} ~ \nonumber\\&
\sinc\left(r-\frac{\ln\alpha^\prime}{\ln{a_\star}}\right) ~ 
\sinc\left(\ell-\frac{a_r \tau^\prime}{T_\star}\right) ~ 
d\alpha^\prime d\tau^\prime.
\end{align}
The parameters $a_\star$ and $T_\star$ are referred to as
the basic scaling factor and the translational spacing, respectively\footnote{The details of $a_\star$ and $T_\star$ will be given in the final report.}.

\section{Multi-layer Transmission}
\label{sec:multi-layer}
%In the multi-layer transmission scheme, 
%the transmit data symbols of the $k^\prime$th layer are modulated by a pulse $p_{k^\prime}(t)$, and up-converted to a carrier frequency $a^{k^\prime} f_c$. 
%At the receiver, the received signal is applied to bank of filters $p_k^*(-t)$, $k=0,1,2,\ldots,K$, then sampled at the appropriate rate at each filter output.

This section describes the multi-layer transmission scheme.
Suppose the transmitted signal $x(t)$ has $K^\prime$ layers such that
\begin{align}
x(t) = \sum_{k^\prime=0}^{K^\prime-1} x_{k^\prime}(t)
\end{align}
in which the signal of the $k^\prime$ layer is
\begin{align}
x_{k^\prime}(t) = \Re\left\{ \bar{x}_{k^\prime}(t) e^{j 2\pi a^{k^\prime} f_c t} \right\}
\end{align}
at carrier frequency $a^{k^\prime} f_c$ and baseband counterpart
\begin{align}
\bar{x}_{k^\prime}(t) = \sum_{n=1}^{N_{k^\prime}} x_{k^\prime}[n] p_{k^\prime}(t-n T/a^{k^\prime})
\end{align}
where $p_{k^\prime}(t)$ is the pulse shape of the $k^\prime$th layer given by
\begin{align}
p_{k^\prime}(t) = a^{k^\prime/2} p(a^{k^\prime} t)
\end{align}
where $p(t)$ is a unit energy pulse whose support in the frequency domain is $[-\frac{W_\star}{2},\frac{W_\star}{2}]$.
The parameters $a$ and $T$ are called the \emph{base scale} and the \emph{base lag}, respectively.
%-------------------------------------------------------------------------------------------------
The received signal $r(t)$ is downconverted using frequencies $a^k f_c$, where $k=0,1,2,\ldots,K$ ($K \geq K^\prime$), 
and the $k$th copy is fed to bank of filters $p_k^*(-t)$, then the output is sampled at the appropriate rate.
The $m$th output of the $k$th filter at the receiver is
\begin{align}
y_{k}[m] 
&= \left. \left [r(t) e^{-j2\pi f_c a^k t} \right] \oplus p_k^*(-t) \right|_{t= m T/a^k} \\
&= \int r(t) e^{-j2\pi f_c a^k t} ~ p_k^*\left(t-m\frac{T}{a^k}\right) dt
\end{align}
where $\oplus$ denotes linear convolution.

%\begin{align}
%\bar{y}_{k^\prime \rightarrow k}[m] 
%= \left. r_{k^\prime}(t) \star p_k^*(-t) \right|_{t= m T/a^k}
%\end{align}

\subsection*{Crosstalk vs. crosstalk-free}
It is shown in \cite{MSML-TSP2013} that if
\begin{align}
a \geq \frac{2 f_c+W_\star}{2 f_c-W_\star},
\end{align}
the crosstalk between layers is eliminated.
An example of a design that satisfies this condition is also given in \cite{MSML-TSP2013}, namely:
\begin{align}
p(t) = \sqrt{W} \sinc(W t)
\end{align}
with $f_c = 1.5 W$ ($W = W_\star$) and $a = 2$.
I plan to compute the information rate for this case and 
compare to that of a case where the condition is violated, e.g., using $a = 1.5$.
The next section outlines how to compute the information rates.

\section{Information Rates}
\label{sec:info_rates}
Let $\x_{k^\prime} = (x_{k^\prime}[1],x_{k^\prime}[2],\ldots,x_{k^\prime}[N_{k^\prime}])$ 
and $\y_k = (y_k[1],y_k[2],\ldots,y_k[N_{k^\prime}])$, i.e.,
$\x_{k^\prime}$ is a vector of all transmitted data symbols of the $k^\prime$th layer and
$\y_k$ is a vector of all output samples of the $k$th receiver filter.
%Let $\X_{k^\prime} = (X_{k^\prime}[1],X_{k^\prime}[2],\ldots,X_{k^\prime}[N_{k^\prime}])$ 
%and $\Y_k = (Y_k[1],Y_k[2],\ldots,Y_k[N_{k^\prime}])$.
The information rate will be computed using
the mutual information between the data symbols of all transmitted layers and the output samples of all receiver filters.
Namely, the rate is\footnote{The uppercase letters $X$ and $Y$ denote random variables 
whose realizations are denoted by the lower case letters $x$ and $y$, respectively.}
\begin{align}
R_{JD} = I(\X_1,\X_2,\ldots,\X_{K^\prime}; \Y_1, \Y_2, \ldots, \Y_K).
\label{eq:rate_jointenc_jointdec}
\end{align}
The rate $R_{JD}$, given in (\ref{eq:rate_jointenc_jointdec}), is the rate achieved by allowing coding across layers 
and joint decoding of all output samples of the receiver filters.
Another strategy is to encode layers independently and process the outputs of different filters individually, i.e.,
use the outputs of the $k$th filter to decode the $k$th layer. In such case, the rate is given by
\begin{align}
R_{ID} = \sum_{k^\prime=0}^{K^\prime-1} I(\X_{k^\prime}; \Y_{k^\prime}).
\label{eq:rate_indivenc_indivdec}
\end{align}
It is easy to show that $R_{JD} \geq R_{ID}$. 
It would be interesting to quantify the gap between $R_{JD}$ and $R_{ID}$ using numerical simulations.


%\appendices
\bibliographystyle{unsrt}
\bibliography{uac1}

\end{document}


