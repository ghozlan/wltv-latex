\documentclass[journal]{IEEEtran}
%\onecolumn
\twocolumn
\input{header}
\input{header_slides}

\begin{document}

\title{%EE599 Underwater Acoustic Communication:\\
Multi-Layer Transmission for Wideband Linear Time-Varying (Gaussian) Channels: 
Interim Report}
\maketitle
%\input{some_file}

\section{Wideband LTV Channel}
Suppose the transmitter emits a (passband) signal $x(t)$. 
The signal $x(t)$ travels through a linear-time varying (LTV) channel
and is corrupted by additive noise.
The receiver observes
\begin{align*}
r(t) = s(t) + n(t)
\end{align*}
where $n(t)$ is a realization of a white Gaussian process with one-sided power spectral density $N_0$
and $s(t)$ is the output of an LTV system.
The wideband representation of a linear time-varying (LTV) system is
\begin{align*}
s(t) = \iint \hwb(\alpha,\tau) ~\sqrt{\alpha} x(\alpha(t-\tau)) ~ d\alpha d\tau
\end{align*}
where $\hwb(\alpha,\tau)$ is the wideband spreading function.
By doing the transformation of variables $\alpha(t-\tau) = \tau^\prime$, we can write
\begin{align*}
s(t) = \int k_0(t,\tau^\prime) x(\tau^\prime) ~ d\tau^\prime
\end{align*}
where the kernel $k_0(t,\tau)$ is related to the wideband spreading function $\hwb(\alpha,\tau)$ via
\begin{align*}
k_0(t,\tau) = \int \hwb\left(\alpha,t-\frac{\tau}{\alpha}\right) \frac{d\alpha}{\sqrt{\alpha}}
\end{align*}
Note: If $k_0(t,\tau) = k_0(t-\tau,0)$, i.e., $k_0(t,\tau)$ depends on $t-\tau$ only, then 
the system is linear time-invariant (LTI) with impulse response $h_{\text{LTI}}(\tau) = k_0(\tau,0)$.

For the simulation, all signals are truncated to $t \in [0,\Tsim]$.
Define $\Nsim = \lfloor \Tsim / \Delta \rfloor$, which is the number of samples in the simulation interval $[0,\Tsim]$.
We have\footnote{The smaller the sampling interval $\Delta$ is, the better the approximation.}
\begin{align*}
s(i\Delta) \approx \sum_{j=1}^{\Nsim} k_0(i \Delta,j \Delta) \Delta ~ x(j \Delta), \quad i=1,\ldots,\Nsim
\end{align*}
which can be written in matrix notation as
\begin{align*}
\s_t = \HCH \x_t
\end{align*}
where $\x_t \in \mathbb{R}^{\Nsim \times 1}$, $\HCH \in \mathbb{R}^{\Nsim \times \Nsim}$ and $\s_t \in \mathbb{R}^{\Nsim \times 1}$
are defined as
\begin{align*}
\s_t &= ( s(\Delta), s(2\Delta), \ldots, s(\Nsim \Delta) )^T \\
\x_t &= ( x(\Delta), x(2\Delta), \ldots, x(\Nsim \Delta) )^T \\
\HCH(i,j) &= k_0(i \Delta,j \Delta) \Delta, \quad i,j=1,\ldots,\Nsim.
\end{align*}

\section{Multi-layer Transmitter}

This section describes the multi-layer transmission scheme.
The transmitted signal $x(t)$ has $K^\prime$ layers such that
\begin{align}
x(t) = \sum_{k^\prime=0}^{K^\prime-1} x_{k^\prime}(t)
\end{align}
in which the signal of the $k^\prime$ layer is
\begin{align}
x_{k^\prime}(t) = \Re\left\{ \bar{x}_{k^\prime}(t) e^{j 2\pi a^{k^\prime} f_c t} \right\}
\end{align}
at carrier frequency $a^{k^\prime} f_c$ and baseband counterpart
\begin{align}
\bar{x}_{k^\prime}(t) = \sum_{n=1}^{N_{k^\prime}} x_{k^\prime}[n] p_{k^\prime}(t-n T/a^{k^\prime})
\end{align}
where $p_{k^\prime}(t)$ is the pulse shape of the $k^\prime$th layer given by
\begin{align}
p_{k^\prime}(t) = a^{k^\prime/2} p(a^{k^\prime} t)
\end{align}
where $p(t)$ is a unit energy pulse whose support in the frequency domain is $[-\frac{W}{2},\frac{W}{2}]$.
The parameters $a$ and $T$ are called the \emph{base scale} and the \emph{base lag}, respectively.


Write the signal $x_{k^\prime}(t)$ of layer $k^\prime$ as
\begin{align*}
x_{k^\prime}(t) 
&= \sum_{n=1}^{N_{k^\prime}} \Re\Big\{ {g_{k^\prime,n}(t)} \ x_{k^\prime}[n] \Big\} \\
&= \sum_{n=1}^{N_{k^\prime}} 
\Big[
\Re\{+g_{k^\prime,n}(t)\} \Re\{x_{k^\prime}[n]\}
~+~ \nonumber\\&\qquad\quad\ 
\Im\{-g_{k^\prime,n}(t)\} \Im\{x_{k^\prime}[n]\}
\Big]
\end{align*}
where
\begin{align*}
g_{k,n}(t) 
= e^{j 2\pi a^{k} f_c t}  ~ a^{k/2} p(a^{k} t-n T).
\end{align*}
%Define the vector $\x_{k}$ of the transmitted symbol of layer $k$ as
%\begin{align*}
%\x_{k} = 
%(
%\underbrace{\Re\{x_{k}[1]\},\ldots,\Re\{x_{k}[\NTXk{k}]\}}_{\text{in-phase}}
%,
%\underbrace{\Im\{x_{k}[1]\},\ldots,\Im\{x_{k}[\NTXk{k}]\}}_{\text{quadrature}}
%)^T
%\end{align*}
Define the vector $\x_{k^\prime}$ of the transmitted symbol of layer $k^\prime$ as
\begin{align*}
\x_{k^\prime} = 
(&
\overbrace{\Re\{x_{k^\prime}[1]\},\ldots,\Re\{x_{k^\prime}[\NTXk{k^\prime}]\}}^{\text{in-phase}}
, \nonumber\\&
\underbrace{\Im\{x_{k^\prime}[1]\},\ldots,\Im\{x_{k^\prime}[\NTXk{k^\prime}]\}}_{\text{quadrature}}
)^T
\end{align*}
Define
\begin{align*}
%\HTXk{k} = [ \g_{I,k,1}, \ldots, \g_{I,k,N_{k}}, \g_{Q,k,1}, \ldots, \g_{Q,k,N_{k}} ] 
\HTXk{k} = [ 
\g_{I,k,1} ~,~ \ldots ~,~ \g_{I,k,\NTXk{k}} ~,~ 
\g_{Q,k,1} ~,~ \ldots ~,~ \g_{Q,k,\NTXk{k}} ] 
\end{align*}
where
\begin{align*}
\g_{I,k,n} &= ( \Re\{+g_{k,n}(\Delta)\}, \ldots, \Re\{+g_{k,n}(\Nsim \Delta)\} )^T \\
\g_{Q,k,n} &= ( \Im\{-g_{k,n}(\Delta)\}, \ldots, \Im\{-g_{k,n}(\Nsim \Delta)\} )^T.
\end{align*}
We can write
\begin{align*}
\x_t  = \sum_{k^\prime=1}^{K^\prime} \HTXk{k^\prime} ~ \x_{k^\prime}
\end{align*}
We collect the symbols of all layers in a vector $\x$ defined as
\begin{align*}
\x = \text{vertcat}(\x_1, \ldots, \x_{K^\prime}) = (\x_1^T, \ldots, \x_{K^\prime}^T)^T
\end{align*}
and collect all matrices in $\HTX$
\begin{align*}
\HTX = [\HTXk{1}, \ldots, \HTXk{K^\prime}].
\end{align*}

We can now write
\begin{align*}
\x_t = \HTX \x
\end{align*}
and
\begin{align*}
\rv_t
&= \s_t + \n_t \\
&= \HCH \x_t + \n_t \\
&= \HCH \HTX \x + \n_t
\end{align*}
where
$\n_t$ is real Gaussian random vector ($\Nsim \times 1$) with mean zero and covariance matrix
\begin{align*}
%\Sigma_{nn} = \frac{1}{\Delta} \frac{N_0}{2} \textbf{I}_{\Nsim \times \Nsim}.
\Sigma_{nn} = \sigma^2 \IM
\end{align*}
where
\begin{align*}
\sigma^2 = \frac{N_0}{2} \frac{1}{\Delta}
\end{align*}
and $\IM$ is the identity matrix ($\Nsim \times \Nsim$).
Recall that
\begin{align*}
\mathbb{E}[|n(t)|^2] = \frac{N_0}{2} \delta_D(t)
\end{align*}
where $\delta_D(\cdot)$ is the Dirac delta function
and also recall that
\begin{align*}
\int \delta_D(t) dt = 1.
\end{align*}

Suppose that there is a power constraint $P$ on the transmitted signal $x(t)$, i.e.,
\begin{align*}
\mathbb{E}\left[ \int_0^\Tsim |x(t)|^2 dt \right] \leq P ~ \TTX.
\end{align*}	
For the simulation, the power constraint can be approximated using
(\alert{see appendix})
\begin{align*}
\tr\left(\HM^\dagger\HM \Sigma_{xx}\right) \Delta \leq P ~ \TTX.
\end{align*}	



\section{Receiver Structure for Multi-layer Transmission}
The receiver has $K \geq K^\prime$ filters (branches).
%The received signal $r(t)$ is downconverted using frequencies $a^k f_c$, where $k=0,1,2,\ldots,K$ ($K \geq K^\prime$), 
%and the $k$th copy is fed to bank of filters $p_k^*(-t)$, then the output is sampled at the appropriate rate.
The $m$th output of the $k$th filter at the receiver is
\begin{align*}
y_{k}[m] 
&= \left. \left[r(t) e^{-j2\pi f_c a^k t} \right] \oplus p_k^*(-t) \right|_{t= m T/a^k} \\
%&= \int r(t) e^{-j2\pi f_c a^k t} ~ p_k^*\left(t-m\frac{T}{a^k}\right) dt \\
&= \int r(t) \Big[ \underbrace{e^{j2\pi f_c a^k t} ~ p(a^k t-m T)}_{g_{k,m}(t)}\Big]^* dt
\end{align*}
where $\oplus$ denotes linear convolution.
Therefore
\begin{align*}
\Re\{y_{k}[m]\} &= \int \Re\{ g_{k,m}(t)\} r(t) dt \\ 
\Im\{y_{k}[m]\} &= \int \Im\{-g_{k,m}(t)\} r(t) dt
\end{align*}
for $m = 1, \ldots, \NRXk{k}$.
Define the vector $\y_k$ of the samples of the output of receiver filter $k$ as
\begin{align*}
\y_k = 
(
\underbrace{\Re\{y_k[1]\},\ldots,\Re\{y_k[\NRXk{k}]\}}_{\text{in-phase}}
, 
\underbrace{\Im\{y_k[1]\},\ldots,\Im\{y_k[\NRXk{k}]\}}_{\text{quadrature}}
)^T
\end{align*}
where $\NRXk{k} \geq \NTXk{k}$. 
\alert{$\NRXk{k}$ should be greater than $\NTXk{k}$ to account for the delay spread. 
However, we use $\NRXk{k} = \NTXk{k}$ in the simulation for simplicity.}
We have
\begin{align*}
y_{k}[m] \approx \sum_{i=1}^{\Nsim} r(i\Delta) [ {g_{k,m}(i\Delta)} ]^* \Delta
\end{align*}
which can be written in matrix notation as
\begin{align*}
\y_k = \HRXk{k}^\dagger ~ \rv_t
\end{align*}
where the receiver matrix $\HRXk{k}$ of filter $k$ is defined as\footnote{
Note that dropping $\Delta$ from $\HRXk{k}$ does not change the performance because because the signal and the noise are scaled.}
\begin{align*}
\HRXk{k} = [ 
\g_{I,k,1} ~, \ldots ,~ \g_{I,k,\NRXk{k}} ~,~ 
\g_{Q,k,1} ~, \ldots ,~ \g_{Q,k,\NRXk{k}} ] ~ \Delta.
\end{align*}

Collecting the all samples of the outputs of the $K$ filters in vector $\y$
\begin{align*}
\y &= \text{vertcat}(\y_1, \ldots, \y_K)
\end{align*}
and collecting all receiver matrices in $\HRX$
\begin{align*}
\HRX = [\HRXk{1}, \ldots, \HRXk{K}]
\end{align*}
allows us to write
\begin{align*}
\y = \HRX^\dagger \rv_t.
\end{align*}
Thus, we have
\begin{align*}
\y = \HM \x + \z
\end{align*}
where
\begin{align*}
\HM &= {\HRX^\dagger \HCH \HTX} \\
\z  &= {\HRX^\dagger \n_t}.
\end{align*}

\section{Design Choices}
We have several design choices to consider.
At the transmitter, we have
\begin{enumerate}
	\item Single-layer ($K^\prime = 1$) transmission
	\item Multi-layer ($K^\prime>1$ layers) transmission with non-overlapping layers %(at transmitter?)
	\item Multi-layer ($K^\prime>1$ layers) transmission with overlapping layers
\end{enumerate}
whereas at the receiver, we have
\begin{enumerate}
	\item Optimal (all samples)
	\item $K$-branch, expanded band ($K > K^\prime$) with joint decoding
	\item $K$-branch, same band ($K = K^\prime$) with joint layer decoding
	\item $K$-branch, same band ($K = K^\prime$) with individual layer decoding.
\end{enumerate}


\section{Information Rate}
In this work, 
we assume that the channel is deterministic 
and is known to the transmitter and the receiver.

\subsection{Same Band Receiver with Joint Layer Decoding}
In this case, the information rate is
\begin{align*}
R_{SB,JLD} 
&= \frac{1}{\TTX} I(\x;\y) \qquad \text{ (bits/second). }
\end{align*}
We have
\begin{align*}
I(\x;\y) 
&= h(\y) - h(\y|\x) \\
&= h(\y) - h(\z) \\
&\leq \frac{1}{2} [ \log\det(\Sigma_{yy}) - \log\det(\Sigma_{zz})]
\end{align*}
where
\begin{align*}
\Sigma_{yy} &= \mathbb{E}[\y \y^\dagger] = \HM \Sigma_{xx} \HM^\dagger + \Sigma_{zz} \\
\Sigma_{zz} &= \HRX^\dagger \Sigma_{nn} \HRX \\
\Sigma_{xx} &= \mathbb{E}[\x \x^\dagger]
\end{align*}
The equality holds if $\x$ is Gaussian.
Therefore, we use a Gaussian codebook to maximize the mutual information.
\alert{However, we do not maximize over set the covariance matrices $\Sigma_{xx}$ satisfying the power constraint.}
We choose uniform power allocation
\footnote{When there are overlapping layers, one has to explain more precisely what ``uniform power allocation'' is done. This will be elaborated in the final report.}
for simplicity.
\alert{It is assumed here that $\Sigma_{zz}$ is non-singular.}

\subsection{Same Band Receiver with Individual Layer Decoding}
In this case, the information rate is
\begin{align*}
R_{SB,ILD} = \frac{1}{\TTX} \sum_{k^\prime=1}^{K^\prime} I(\x_{k^\prime}; \y_{k^\prime})
\qquad \text{ (bits/second). }
\end{align*}

\subsection{Optimal Receiver}
The optimal receiver uses the \emph{entire} received signal to decode the transmitted codeword.
The information rate of the optimal receiver is
\begin{align*}
R_{opt} = \frac{1}{\TTX} I(\x;\rv_t) \qquad \text{ (bits/second). }
\end{align*}
We have
\begin{align*}
I(\x;\rv_t) = \frac{1}{2} [ \log\det(\Sigma_{rr}) - \log\det(\Sigma_{nn})]
\end{align*}
where %$\Sigma_{rr} = \Sigma_{ss} + \Sigma_{nn}$
\begin{align*}
\Sigma_{rr} = \Sigma_{ss} + \Sigma_{nn} = \HM \Sigma_{xx} \HM^\dagger + \Sigma_{nn}
\end{align*}
Since $\Sigma_{ss}$ is symmetric (normal matrix), then it is unitarily diagonalizable, i.e.,
\begin{align*}
\Sigma_{ss} = \U_s \Lambda_s \U_s^T
\end{align*}
where $\U_s \U_s^T = \IM$ and $\Lambda_s$ is a diagonal matrix.
Therefore,
\begin{align*}
\Sigma_{ss} + \Sigma_{nn} 
&= \U_s \Lambda_s \U_s^T + \sigma^2 \U_s \U_s^T \\
&= \U_s (\Lambda_s + \sigma^2 \IM) \U_s^T
\end{align*}
which implies
\begin{align*}
\det(\Sigma_{ss} + \Sigma_{nn})
&= \det(\U_s) \det(\Lambda_s + \sigma^2 \IM) \det(\U_s^T) \\
&= \det(\Lambda_s + \sigma^2 \IM).
\end{align*}
Therefore, we have
\begin{align*}
I(\x;\rv_t) = \frac{1}{2} [ \det(\Lambda_s + \sigma^2 \IM) - \log\det(\sigma^2 \IM)].
\end{align*}
Note here that note that both determinants are (strictly) positive.

\paragraph*{Remark}
It can be shown that
%\begin{align*}
%C
%&\geq I(\x_1,\x_2,\ldots,\x_{K^\prime}; \rv_t) \\
%&\geq I(\x_1,\x_2,\ldots,\x_{K^\prime}; \y_1, \y_2, \ldots, \y_K) \\
%&\geq I(\x_1,\x_2,\ldots,\x_{K^\prime}; \y_1, \y_2, \ldots, \y_{K^\prime}) \\
%&\geq \sum_{k^\prime=1}^{K^\prime} I(\x_{k^\prime}; \y_{k^\prime})
%\end{align*}
%
%\begin{align*}
%I(\x; \rv_t)
%&\geq I(\x; \y_1, \y_2, \ldots, \y_K) \\
%&\geq I(\x; \y_1, \y_2, \ldots, \y_{K^\prime}) \\
%&\geq \sum_{k^\prime=1}^{K^\prime} I(\x_{k^\prime}; \y_{k^\prime})
%\end{align*}
\begin{align*}
R_{\text{opt}} \geq R_{\text{EB}} \geq R_{\text{SB,JLD}} \geq R_{\text{SB,ILD}}
\end{align*}
where
\begin{align*}
R_{\text{opt}}
&= \frac{1}{\TTX} I(\x;\rv_t) \\
%--------------------------------------------------------------------------------------------
R_{\text{EB}} 
&= \frac{1}{\TTX} I(\x_1,\x_2,\ldots,\x_{K^\prime}; \y_1, \y_2, \ldots, \y_K) \\
%--------------------------------------------------------------------------------------------
R_{\text{SB,JLD}} 
&= \frac{1}{\TTX} I(\x_1,\x_2,\ldots,\x_{K^\prime}; \y_1, \y_2, \ldots, \y_{K^\prime}) \\
%--------------------------------------------------------------------------------------------
R_{\text{SB,ILD}} 
&= \frac{1}{\TTX} \sum_{k^\prime=1}^{K^\prime} I(\x_{k^\prime}; \y_{k^\prime})
\end{align*}

\section{Numerical Results}
\subsection{Transmission Schemes}
We use
\begin{align}
p(t) = \sqrt{W} \sinc(W t)
\end{align}
with $f_c = 1.5 W$.

Consider the following schemes.
\begin{itemize}
\item
Scheme 1: $W = 1 \ (T=1)$, $a = 2$, $K^\prime = 3$ layers.

\item
Scheme 2: $W = 1 \ (T=1)$, $a = 1.5874$, $K^\prime = 4$ layers.
\end{itemize}
Both schemes: $f_{min} = 1$, $f_{max} = 8$, $B = 7$ (total bandwidth).

It is shown in \cite{MSML-TSP2013} that if
\begin{align}
a \geq \frac{2 f_c+W}{2 f_c-W},
\end{align}
the crosstalk between layers is eliminated.
This condition is satisfied for Scheme 1 but not for Scheme 2.

\subsection{Channel}
We consider channel with a finite number of paths:
\begin{align*}
\hwb(\alpha,\tau) = \sum_m h_m \delta(\alpha-\alpha_m) \delta(\tau-\tau_m).
\end{align*}
Equivalently,
\begin{align*}
k_0(t,\tau) = \sum_m h_m \sqrt{\alpha_m} \delta(t-\tau-\tau_m(t))
\end{align*}
where $\tau_m(t) = \alpha_m \tau_m - (\alpha_m-1) t$. 


Fig. \ref{fig:cha_ild} compares the information rates achieved by two different schemes when a same-band receiver with individual layer decoding is used.
\begin{figure}[ht]
\centering
\includegraphics[width=1.0\columnwidth]{./simfig/CHA_ILD.eps}
\caption{Comparing rates of different schemes using same-band receiver with individual layer decoding)}
\label{fig:cha_ild}
\end{figure}
Channel A 
used to generate this figure is
\begin{align*}
	\hwb(\alpha,\tau) = \delta_D(\alpha-1) \delta_D(\tau) + \delta_D(\alpha-2) \delta_D(\tau-2).
\end{align*}


%Channel B:
%\begin{align}
	%\hwb(\alpha,\tau) = \delta(\alpha-1) \delta(\tau) + \delta(\alpha-1.5874) \delta(\tau-2)
%\end{align}

\appendices
\section{Power Constraint}
We have
\begin{align*}
\int_0^\Tsim |x(t)|^2 dt 
&\approx \sum_{i=1}^{\Nsim} |x(i \Delta)|^2 \Delta \\
&= \x_t^\dagger \x_t ~ \Delta \\
&= \tr\left(\x_t \x_t^\dagger\right) \Delta \\
&= \tr\left(\HM \x \x^\dagger \HM^\dagger\right) \Delta
\end{align*}	
It follows that
\begin{align*}
\mathbb{E}\left[ \int_0^\Tsim |x(t)|^2 dt \right]
&\approx \mathbb{E}\left[ \tr\left(\HM \x \x^\dagger \HM^\dagger\right) \Delta \right] \\
&= \tr\left(\HM \Sigma_{xx} \HM^\dagger\right) \Delta \\
&= \tr\left(\HM^\dagger\HM \Sigma_{xx}\right) \Delta
\end{align*}	
%because $\tr(A~B) = \tr(B~A)$ {\color[rgb]{1,0,0} [ref?]}


\section{Differential Entropy}
For real Gaussian vector $\z \in \mathbb{R}^m$ with covariance $\Sigma_{zz}$,
the differential entropy is
\begin{align*}
h(\z) = \frac{1}{2} [ m \log(2 \pi e) + \log\det(\Sigma_{zz}) ]
\end{align*}
assuming that $\Sigma_{zz}$ is non-singular.
For circularly-symmetric complex\footnote{That is the pseudo-covariance matrix $\mathbb{E}[\z \z^T]$ is equal to the zero matrix.}
Gaussian vector $\z \in \mathbb{C}^m$ with covariance $\Sigma_{zz}$,
the differential entropy is
\begin{align*}
h(\z) = m \log(\pi e) + \log\det(\Sigma_{zz})
\end{align*}
also assuming that $\Sigma_{zz}$ is non-singular.


\bibliographystyle{unsrt}
\bibliography{uac2}

\end{document}