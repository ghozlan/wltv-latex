%% Figures used in the Globecom 2013 paper
LineWidth = 2;
MarkerSize = 10;
FontSize = 17;
FontSizeLegend = 14;
%% 16-QAM, f_HWHM = 0.125, Square TX Pulse, Optimal RX, OSR = {4,8,16}, S = {16,32,64}, Ubersampling (OSR_sim = 1024)
h=figure(1);
SNR_dB = -5:1.25:25;
plot(SNR_dB,qam_rate(4,SNR_dB),'k-','LineWidth',LineWidth,'MarkerSize',MarkerSize)
xlabel('SNR (dB)','FontSize',FontSize)
%ylabel('Rate (bits/symbol)')
ylabel('$\underline{I}(X;Y)$ (bits/symbol)','Interpreter','LaTex','FontSize',FontSize)
axis([-5 25 0 4])
grid on
hold on

directory = '../uber/qam16-sqr/';

list = dir([directory 'uber*aux_sqrtx_optrx_osr16_fhwhm125_qam16_N10000_S64*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'gx-','LineWidth',LineWidth,'MarkerSize',MarkerSize)
list = dir([directory 'uber*aux_sqrtx_optrx_osr16_fhwhm125_qam16_N10000_S32*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'bx-','LineWidth',LineWidth,'MarkerSize',MarkerSize)
list = dir([directory 'uber*aux_sqrtx_optrx_osr16_fhwhm125_qam16_N10000_S16*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'rx-','LineWidth',LineWidth,'MarkerSize',MarkerSize)


list = dir([directory 'uber*aux_sqrtx_optrx_osr8_fhwhm125_qam16_N10000_S64*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'go-','LineWidth',LineWidth,'MarkerSize',MarkerSize)
list = dir([directory 'uber*aux_sqrtx_optrx_osr8_fhwhm125_qam16_N10000_S32*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'bo-','LineWidth',LineWidth,'MarkerSize',MarkerSize)
list = dir([directory 'uber*aux_sqrtx_optrx_osr8_fhwhm125_qam16_N10000_S16*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'ro-','LineWidth',LineWidth,'MarkerSize',MarkerSize)


list = dir([directory 'uber*aux_sqrtx_optrx_osr4_fhwhm125_qam16_N10000_S64*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'g*-','LineWidth',LineWidth,'MarkerSize',MarkerSize)
list = dir([directory 'uber*aux_sqrtx_optrx_osr4_fhwhm125_qam16_N10000_S32*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'b*-','LineWidth',LineWidth,'MarkerSize',MarkerSize)
list = dir([directory 'uber*aux_sqrtx_optrx_osr4_fhwhm125_qam16_N10000_S16*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'r*-','LineWidth',LineWidth,'MarkerSize',MarkerSize)

hold off

legend('AWGN',...
       'L=16, S=64','L=16, S=32','L=16, S=16',...
       'L= 8, S=64','L= 8, S=32','L= 8, S=16',...
       'L= 4, S=64','L= 4, S=32','L= 4, S=16',...
       'Location','NorthWest')

handles = get(1,'Children'); set(handles(1),'FontSize',FontSizeLegend);

print('-depsc', 'uber_qam16_fhwhm125_sqrtx_optrx_long_osr.eps')

title('Square Pulse, Opt. Rx, f_{HWHM}=0.125, 16-QAM, Block Length=10^4, L_{sim}=1024')
print('-depsc', 'uber_qam16_fhwhm125_sqrtx_optrx_long_osr_title.eps')

%% 16-QAM, f_HWHM = 0.125, Cosine^2 TX Pulse, Optimal RX, OSR = {4,8,16}, S = {16,32,64}, Ubersampling (OSR_sim = 1024)   
figure(2)
SNR_dB = -5:1.25:25;
plot(SNR_dB,qam_rate(4,SNR_dB),'k-','LineWidth',LineWidth,'MarkerSize',MarkerSize)
xlabel('SNR (dB)','FontSize',FontSize)
%ylabel('Rate (bits/symbol)')
ylabel('$\underline{I}(X;Y)$ (bits/symbol)','Interpreter','LaTex','FontSize',FontSize)
axis([-5 25 0 4])
grid on
hold on

directory = '../uber/qam16-cos2/';

list = dir([directory 'uber*aux_cos2tx_optrx_osr16_fhwhm125_qam16_N10000_S64*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'gx-','LineWidth',LineWidth,'MarkerSize',MarkerSize)
list = dir([directory 'uber*aux_cos2tx_optrx_osr16_fhwhm125_qam16_N10000_S32*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'bx-','LineWidth',LineWidth,'MarkerSize',MarkerSize)
list = dir([directory 'uber*aux_cos2tx_optrx_osr16_fhwhm125_qam16_N10000_S16*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'rx-','LineWidth',LineWidth,'MarkerSize',MarkerSize)


list = dir([directory 'uber*aux_cos2tx_optrx_osr8_fhwhm125_qam16_N10000_S64*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'go-','LineWidth',LineWidth,'MarkerSize',MarkerSize)
list = dir([directory 'uber*aux_cos2tx_optrx_osr8_fhwhm125_qam16_N10000_S32*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'bo-','LineWidth',LineWidth,'MarkerSize',MarkerSize)
list = dir([directory 'uber*aux_cos2tx_optrx_osr8_fhwhm125_qam16_N10000_S16*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'ro-','LineWidth',LineWidth,'MarkerSize',MarkerSize)


list = dir([directory 'uber*aux_cos2tx_optrx_osr4_fhwhm125_qam16_N10000_S64*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'g*-','LineWidth',LineWidth,'MarkerSize',MarkerSize)
list = dir([directory 'uber*aux_cos2tx_optrx_osr4_fhwhm125_qam16_N10000_S32*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'b*-','LineWidth',LineWidth,'MarkerSize',MarkerSize)
list = dir([directory 'uber*aux_cos2tx_optrx_osr4_fhwhm125_qam16_N10000_S16*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'r*-','LineWidth',LineWidth,'MarkerSize',MarkerSize)

hold off

legend('AWGN',...
       'L=16, S=64','L=16, S=32','L=16, S=16',...
       'L= 8, S=64','L= 8, S=32','L= 8, S=16',...
       'L= 4, S=64','L= 4, S=32','L= 4, S=16',...
       'Location','NorthWest')

handles = get(2,'Children'); set(handles(1),'FontSize',FontSizeLegend);

print('-depsc', 'uber_qam16_fhwhm125_cos2tx_optrx_long_osr.eps')

title('Cosine^2 Pulse, Opt. Rx, f_{HWHM}=0.125, 16-QAM, Block Length=10^4, L_{sim}=1024')
print('-depsc', 'uber_qam16_fhwhm125_cos2tx_optrx_long_osr_title.eps')

%% 16-QAM, f_HWHM = 0.125, Square TX vs Cosine^2 TX Pulse, Optimal RX, OSR = {4,8,16}, S = 32, Ubersampling (OSR_sim = 1024)

figure(3)
SNR_dB = -5:1.25:25;
plot(SNR_dB,qam_rate(4,SNR_dB),'k-','LineWidth',LineWidth,'MarkerSize',MarkerSize)
xlabel('SNR (dB)','FontSize',FontSize)
%ylabel('Rate (bits/symbol)')
ylabel('$\underline{I}(X;Y)$ (bits/symbol)','Interpreter','LaTex','FontSize',FontSize)
axis([-5 25 0 4])
grid on
hold on

directory = '../uber/qam16-sqr/';
list = dir([directory 'uber*aux_sqrtx_optrx_osr16_fhwhm125_qam16_N10000_S32*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'bx-','LineWidth',LineWidth,'MarkerSize',MarkerSize)
list = dir([directory 'uber*aux_sqrtx_optrx_osr8_fhwhm125_qam16_N10000_S32*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'bo-','LineWidth',LineWidth,'MarkerSize',MarkerSize)
list = dir([directory 'uber*aux_sqrtx_optrx_osr4_fhwhm125_qam16_N10000_S32*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'b*-','LineWidth',LineWidth,'MarkerSize',MarkerSize)

directory = '../uber/qam16-cos2/';
list = dir([directory 'uber*aux_cos2tx_optrx_osr16_fhwhm125_qam16_N10000_S32*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'rx-','LineWidth',LineWidth,'MarkerSize',MarkerSize)
list = dir([directory 'uber*aux_cos2tx_optrx_osr8_fhwhm125_qam16_N10000_S32*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'ro-','LineWidth',LineWidth,'MarkerSize',MarkerSize)
list = dir([directory 'uber*aux_cos2tx_optrx_osr4_fhwhm125_qam16_N10000_S32*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'r*-','LineWidth',LineWidth,'MarkerSize',MarkerSize)

hold off

legend('AWGN',...
       'L=16, sqr'  ,'L= 8, sqr'  ,'L= 4, sqr'  ,...
       'L=16, cos^2','L= 8, cos^2','L= 4, cos^2',...
       'Location','NorthWest')
   
handles = get(3,'Children'); set(handles(1),'FontSize',FontSizeLegend);

print('-depsc', 'uber_qam16_fhwhm125_sqrtx_vs_cos2tx_optrx_long_osr.eps')

title('Opt. Rx, f_{HWHM}=0.125, 16-QAM, Block Length=10^4, S=32, L_{sim}=1024')
print('-depsc', 'uber_qam16_fhwhm125_sqrtx_vs_cos2tx_optrx_long_osr_title.eps')

%% 16-PSK, f_HWHM = 0.0125, Square TX Pulse, Optimal RX, OSR = {1,2,4,8,16}, S = 64, Ubersampling (OSR_sim = 1024)

figure(4)
SNR_dB = -5:1.25:25;
plot(SNR_dB,psk_rate(16,SNR_dB),'k-','LineWidth',LineWidth,'MarkerSize',MarkerSize)
xlabel('SNR (dB)','FontSize',FontSize)
%ylabel('Rate (bits/symbol)')
ylabel('$\underline{I}(X;Y)$ (bits/symbol)','Interpreter','LaTex','FontSize',FontSize)
axis([10 30 0 4])
grid on
hold on

directory = '../uber/psk16-sqr/';
list = dir([directory 'uber*aux_sqrtx_optrx_osr16_fhwhm12_psk16_N10000_S64*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'bx-','LineWidth',LineWidth,'MarkerSize',MarkerSize)

list = dir([directory 'uber*aux_sqrtx_optrx_osr8_fhwhm12_psk16_N10000_S64*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'bo-','LineWidth',LineWidth,'MarkerSize',MarkerSize)

list = dir([directory 'uber*aux_sqrtx_optrx_osr4_fhwhm12_psk16_N10000_S64*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'b*-','LineWidth',LineWidth,'MarkerSize',MarkerSize)

list = dir([directory 'uber*aux_sqrtx_optrx_osr2_fhwhm12_psk16_N10000_S64*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'b+-','LineWidth',LineWidth,'MarkerSize',MarkerSize)

list = dir([directory 'uber*aux_sqrtx_optrx_osr1_fhwhm12_psk16_N10000_S64*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'bd-','LineWidth',LineWidth,'MarkerSize',MarkerSize)

% f_HWHM = 0.0125;
% sig2 = 2*pi*(2*f_HWHM);
% entropy_theta_unwrapped = 1/2*log2(2*pi*exp(1)*sig2);
% ub = log2(2*pi) - entropy_theta_unwrapped;
% plot(SNR_dB, ub * ones(1,length(SNR_dB)),'m-','LineWidth',LineWidth,'MarkerSize',MarkerSize)

hold off

legend('AWGN',...
       'L=16',...
       'L= 8',...
       'L= 4',...
       'L= 2',...
       'L= 1',...
       'Location','SouthEast')

handles = get(4,'Children'); set(handles(1),'FontSize',FontSizeLegend);

print('-depsc', 'uber_psk16_fhwhm12_sqrtx_optrx_long_osr.eps')

title('Square Pulse, Opt. Rx, f_{HWHM}=0.0125, 16-PSK, Block Length=10^4, S=64, L_{sim}=1024')
print('-depsc', 'uber_psk16_fhwhm12_sqrtx_optrx_long_osr_title.eps')

%% Comparison: Ghozlan-Kramer vs Martalo-Tripodi-Raheli vs Baud
figure(5)
SNR_dB = -5:1.25:25;
plot(SNR_dB,qam_rate(4,SNR_dB),'ko-','LineWidth',LineWidth,'MarkerSize',MarkerSize)
xlabel('SNR (dB)','FontSize',FontSize)
ylabel('Information Rate (bits/symbol)')
axis([-5 25 0 4])
grid on
hold on

directory = '../uber/compare-baud-mtr/';
list = dir([directory 'uber_fast_aux_sqrtx_optrx_osr16_fhwhm12_qam16_N10000_S64*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'bo-','LineWidth',LineWidth,'MarkerSize',MarkerSize)
directory = '../raheli-data/';
list = dir([directory 'raheli_osr16_fhwhm12_N100000_S128_qam16*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'ro-','LineWidth',LineWidth,'MarkerSize',MarkerSize)
directory = '../baud-data/';
list = dir([directory 'fast_original_fhwhm12_N100000_S128_qam16_*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'go-','LineWidth',LineWidth,'MarkerSize',MarkerSize)

SNR_dB = -5:1.25:25;
plot(SNR_dB,qam_rate(2,SNR_dB),'kx-','LineWidth',LineWidth,'MarkerSize',MarkerSize)

directory = '../uber/compare-baud-mtr/';
list = dir([directory 'uber_fast_aux_sqrtx_optrx_osr16_fhwhm19_qam4_N10000_S128*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'bx-','LineWidth',LineWidth,'MarkerSize',MarkerSize)
directory = '../raheli-data/';
list = dir([directory 'raheli_osr16_fhwhm19_N100000_S128_qam4*.mat']);   
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'rx-','LineWidth',LineWidth,'MarkerSize',MarkerSize)
directory = '../baud-data/';
list = dir([directory 'fast_original_fhwhm19_N100000_S128_qam4*.mat']);
[i_x_y, SNR_dB] = get_stats(directory,list);
plot(SNR_dB,i_x_y.avg,'gx-','LineWidth',LineWidth,'MarkerSize',MarkerSize)


% sig2 = 2*pi*(2*f_HWHM);
sig2 = 0.5^2;
entropy_theta_unwrapped = 1/2*log2(2*pi*exp(1)*sig2);
ub = log2(2*pi) - entropy_theta_unwrapped
% plot(SNR_dB, ub * ones(1,length(SNR_dB)),'m-','LineWidth',LineWidth,'MarkerSize',MarkerSize)

legend('16-QAM, AWGN',...
       '16-QAM, \gamma = 0.4, GK',...
       '16-QAM, \gamma = 0.4, MTR',...
       '16-QAM, \gamma = 0.4, Baud',...
       '  QPSK, AWGN',...
       '  QPSK, \gamma = 0.5, GK',...
       '  QPSK, \gamma = 0.5, MTR',...       
       '  QPSK, \gamma = 0.5, Baud',...
       'Location','NorthWest')
   
hold off

handles = get(5,'Children'); set(handles(1),'FontSize',FontSizeLegend);

print('-depsc', 'comparison.eps')

title('Square Pulse')
print('-depsc', 'comparison_title.eps')

