function [output SNRdB] = get_stats(directory,list)

load([directory list(1).name],'SNR_dB')

%SNR_dB = -5:2.5:25;
i_x_y_vec_min =  Inf * ones(1,length(SNR_dB));
i_x_y_vec_max = -Inf * ones(1,length(SNR_dB));
i_x_y_vec_avg = zeros(1,length(SNR_dB));
i_x_y_vec_var = zeros(1,length(SNR_dB));

n = 0;
for j=1:length(list)
    filename = [directory list(j).name];
    load(filename,'remaining_sim_time','i_x_y_vec');
    if(remaining_sim_time == 0)
        n = n + 1;
        
        i_x_y_vec_max = max(i_x_y_vec_max, i_x_y_vec);
        %i_x_y_vec(i_x_y_vec==0) = Inf;
        i_x_y_vec_min = min(i_x_y_vec_min, i_x_y_vec);
        
        i_x_y_vec_avg = i_x_y_vec_avg + i_x_y_vec;
    end
end
i_x_y_vec_avg = i_x_y_vec_avg/n;

i_x_y.max = i_x_y_vec_max;
i_x_y.min = i_x_y_vec_min;
i_x_y.avg = i_x_y_vec_avg; 


for j=1:length(list)
    filename = [directory list(j).name];
    load(filename,'remaining_sim_time','i_x_y_vec');
    if(remaining_sim_time == 0)
        n = n + 1;
        i_x_y_vec_var = i_x_y_vec_var + (i_x_y_vec-i_x_y_vec_avg).^2;
    end
end
i_x_y_vec_var = i_x_y_vec_var/n;
i_x_y.var = i_x_y_vec_var; 

output = i_x_y;
SNRdB = SNR_dB;