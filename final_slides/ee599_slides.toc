\beamer@endinputifotherversion {3.27pt}
\beamer@sectionintoc {2}{Wideband Representation of Linear Time-Varying Channels}{2}{0}{1}
\beamer@sectionintoc {3}{Multi-layer Transmission Scheme}{7}{0}{2}
\beamer@sectionintoc {4}{Information Rates}{15}{0}{3}
\beamer@sectionintoc {5}{Numerical Simulation}{23}{0}{4}
